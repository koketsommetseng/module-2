// App Name : Digger
// Sector/Category : Online Employment Marketplace
// Year it Won: 2019
// Developer : Lindi Engelbrecht

void main() {
  var app1 = app();
  app1.name = "Digger";
  app1.category = "Online Employment Marketplace";
  app1.year = 2019;
  app1.developer = "Lindi Engelbrecht";

  print(
      "The ${app1.name} app of the ${app1.category} category, won the ${app1.year} app of the year and was developed by ${app1.developer}");
}

class app {
  var name;
  var category;
  var year;
  var developer;
}
