// Subject: Digger
// Goal: Transform the print of "Digger" into captial letters = "DIGGER"

void main() {
  var app1 = app();
  app1.name = "Digger";
  app1.category = "Online Employment Marketplace";
  app1.year = 2019;
  app1.developer = "Lindi Engelbrecht";
  String appInCapitalLetters = "DIGGER";
  print(
      "The ${app1.name} app of the ${app1.category} category, won the ${app1.year} app of the year and was developed by ${app1.developer}");
  print("\n");
  print("Change ${app1.name} into $appInCapitalLetters");
}

class app {
  var name;
  var category;
  var year;
  var developer;

  void appInCapitalLetters(String name) {}
}
